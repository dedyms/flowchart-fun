FROM registry.gitlab.com/dedyms/node:14-dev AS tukang
USER $CONTAINERUSER
WORKDIR $HOME
RUN git clone --depth=1 https://github.com/tone-row/flowchart-fun.git flowchart-fun
WORKDIR /home/$CONTAINERUSER/flowchart-fun
#RUN npm install yarn && ls -la $HOME/.config && yarn install
RUN yarn install --network-timeout 1000000

FROM registry.gitlab.com/dedyms/node:14
RUN npm install -g concurrently && rm -rf $HOME/.config
ENV NODE_ENV=production
COPY --from=tukang --chown=$CONTAINERUSER:$CONTAINERUSER /home/$CONTAINERUSER/flowchart-fun/ /home/$CONTAINERUSER/flowchart-fun/
WORKDIR /home/$CONTAINERUSER/flowchart-fun
USER $CONTAINERUSER
CMD ["yarn", "start"]
